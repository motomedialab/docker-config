# Configuring SSL locally

By default, this Nginx setup generates self-signed SSL certificates using
[mkcert](https://github.com/FiloSottile/mkcert) within the nginx container.

The first time you bring up a project with this configuration, a directory will be created
on your local machine `~/.mkcert-keys` and a root authority certificate generated and installed
into this folder.

### Configure docker

You will need to ensure that your `nginx` service has the appropriate SSL_DOMAINS environment
variable for your project. This is a comma separated list of domains, which can optionally be wildcards.

### Configure your machine

To make your machine respect these SSL certificates, you need to install the root authority certificate. This
can be done using a simple command:

**MacOS:** `sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain ~/.mkcert-keys/rootCA.pem`

### Compatibility with Vite

Generated certificates are mapped to the projects `docker/certificates` directory. To make Vite compatible with SSL,
you need to add the below:

```
server: {
    https: {
        key: './docker/certificates/dev.key',
        cert: './docker/certificates/dev.pem',
    },
    host: '0.0.0.0',
    hmr: {
        host: 'localhost'
    },
},
```