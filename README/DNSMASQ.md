# Using dnsmasq

To configure wildcard `*.test` domains, we need to use a local DNS server to point any requests to our local machine.

1. Install dnsmasq

```shell
brew install dnsmasq
```
 
2. Append the dnsmasq config directory to base dnsmasq configuration file

```shell
echo "conf-dir=/usr/local/etc/dnsmasq.d" >> /usr/local/etc/dnsmasq.conf
```

3. Create custom dnsmasq configuration `/usr/local/etc/dnsmasq.d/test-wildcard.conf` with the below contents:

```shell
server=192.168.1.1 # fallback 1 - Motocom firewall
server=8.8.8.8 # fallback 2 - Google DNS
address=/test/127.0.0.1 # route *.test to localhost
listen-address=127.0.0.1 # listen on local
``` 

4. Restart dnsmasq

```shell
sudo brew services restart dnsmasq
```

5. Configure mac network to use `127.0.0.1` as the DNS server. Any requests will be routed
6. to the dnsmasq service and will use the fallback DNS servers defined in the config.