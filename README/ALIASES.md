# Setting up aliases in MAC

If your shell uses zsh (which it should do), append a source file to your root zsh configuration file `~/.zshrc`:

```shell
echo "\n\nsource ~/.aliases" > ~/.zshrc
```

To create your aliases, all you need to do is set them up inside a `~/.aliases` file.
Now, when opening a new terminal window, the aliases file will be read and aliases setup.
An example of useful Docker aliases is below:

```shell
# docker aliases
alias dc="docker compose"
alias pa="dc exec php-fpm php artisan"
alias dcomposer="dc exec php-fpm composer"
```

With the above configuration, running `pa route:list` will send the artisan command `route:list` to
the `php-fpm` docker container and output a list of routes. With this, it's possible to run your setup
without actually having PHP (or that specific PHP version) installed locally.