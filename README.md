# Docker / Laravel development environment

This project contains a basic Docker setup with two key elements. The `docker` directory,
and the `docker-compose.yml` file. Both should be pasted directly into the
*base directory* of a Laravel project.

With Docker Compose, the command `docker compose up` can be run which will simply setup everything required to develop a
Laravel application. The container versions have been carefully chosen to match Motocom production environments as
closely as possible.

## Docker Services

The services below are configured however additional services can be added to the docker-compose file on a per-project basis.

| Service name | Purpose                                                                            |
|--------------|------------------------------------------------------------------------------------|
| nginx        | The web server to serve files                                                      |
| php-fpm      | The PHP handler                                                                    |
| redis        | The 'cache and database-as-a-service' server                                       |
| mariadb      | The database server                                                                |
| mailhog      | For development purposes only, it captures emails for preview and testing purposes |

## Communicating with containers

To be able to communicate with the containers locally, you need the following line in your hosts file -

```
127.0.0.1 nginx mariadb redis mailhog php-fpm
```

This tells your development machine that the above hostnames are accessible on `127.0.0.1`.

Inside the Docker containers, Docker automatically sets up DNS resolution so that `mariadb` for example, points to
the `mariadb` Docker container.

## The Docker directory

`docker/nginx/nginx.conf` provides a basic configuration file for nginx to use. The docker-compose file then maps
this to the nginx container, providing the necessary configuration to boot up and work successfully.

This file tells nginx to serve static files and route any PHP requests to the PHP-FPM container.

`docker/php-fpm/Dockerfile` takes our base PHP 8.1 image, downloads composer and installs pcov - a code coverage tool
for debugging purposes.

`docker/php-fpm/php.ini` sets PHP configuration such as file sizes that can be uploaded and disables opcache. Without the
opcache option being disabled, changes to files wouldn't be recognised.

## Configuring .test TLD for development

To resolve wildcard test TLDs against the local machine, `dnsmasq` will need to be installed.
More information can be [found here](README/DNSMASQ.md).

## SSL certificates

This setup uses mkcert to generate local SSL certificates which can be accepted by your machine.
More information can be [found here](README/SSL.md).

## Aliases

For quicker development, it is worth setting up aliases to run commands against the docker containers.
More information on setting up aliases can be [found here](README/ALIASES.md).